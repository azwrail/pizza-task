**Hello! Welcome to Cipolla-pizza! This application allows you to feel yourself an owner a pizza delivery business and a pizza customer in one!**

**1. Technology**

- Backend - Laravel.
- Database - MySQL.
- Frontend - Bootstrap.
- Deployment - Heroku.
- Db host - ClearDB.
- Code Source Management - Git with Gitlab.com

**2. Utilization**

- To start this application like a business owner please while Signing-Up don't forget to input
a secret key which is: **_Innoscripta_**. You will be authorized and have acсess to a CRM system
that allows you to manage your pizza-shop! You can add new pizza, delete old pizza, check
orders from customers and manage of order's status. While adding a new pizza you are not required
to enter a link to image of pizza, cause application has a default image for all new pizzas which
are the logo of your shop. Great, isn't it!


- To start this application like a customer please do not hesitate to add desirable pizzas right
into your shopping cart. The application using friendly interface will help you step by step to
do the order. The login is not required but if so you have the opportunity to check your own history of orders. Have a good shopping!
