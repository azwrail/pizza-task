<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 * @package App\Models
 *
 * @property int $id
 * @property int $product_id
 * @property string $path
 */
class Image extends Model
{
    use softDeletes;

    protected $fillable = ['id', 'path', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
