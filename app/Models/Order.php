<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property bool $is_completed
 * @property string $address
 * @property string $details
 */
class Order extends Model
{
    protected $fillable = ['id', 'user_id', 'is_completed', 'user_phone', 'address', 'details'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'order_products', 'order_id', 'product_id')->withTrashed()->withPivot('quantity');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function calculateFullSum()
    {
        $sum = 0;

        foreach ($this->products()->get() as $product) {
            $sum += ($product->price)*($product->pivot->quantity);
        }

        return $sum;
    }

}
