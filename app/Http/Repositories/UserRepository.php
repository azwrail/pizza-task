<?php


namespace App\Http\Repositories;


use App\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{

    private $secretKey;

    /**
     * UserRepository constructor.
     * Set a private key for admin
     */
    public function __construct()
    {
        $this->secretKey = 'Innoscripta';
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $name
     * @param string|null $key
     * @return User
     */
    public function create(string $email, string $password, string $name, string $key = null): User
    {

        $user = new User;

        $user->name = $name;
        $user->email = $email;

        $this->checkSecretKey($key, $user);
        $user->password = Hash::make($password);

        $user->save();

        return $user;
    }

    /**
     * @param int $id
     * @return User
     */
    public function find(int $id): User
    {
        return User::find($id);
    }

    /**
     * @param User $user
     * @param string $phone
     * @return User
     */
    public function update(User $user, string $phone): User
    {
        $user->phone = $phone;

        $user->save();

        return $user;
    }

    /**
     * @param string|null $key
     * @param User $user
     * @return int
     */
    public function checkSecretKey(?string $key, User $user)
    {
        if ($key == $this->secretKey) {
            return $user->is_admin = 1;
        }

        return $user->is_admin = 0;
    }
}
