<?php


namespace App\Http\Repositories;


use App\Cart;
use App\Models\Order;
use App\Models\OrderProduct;

class OrderRepository
{
    /**
     * @param array $productsId
     * @param int $user_id
     * @param string $user_phone
     * @return Order
     */
        public function store(string $phone, string $address, string $details = null, Cart $cart): Order
        {
        $order = new Order();

        if ($user = auth()->user()) {
            $order->user_id = $user->id;
        }
        $order->user_phone = $phone;
        $order->address = $address;
        if ($details) {
            $order->details = $details;

        }
        $order->is_completed = 0;

        $order->save();

        foreach ($cart->getItems() as $cartItem) {

            $orderProduct = new OrderProduct();

            $orderProduct->order_id = $order->id;
            $orderProduct->product_id = $cartItem['item']->id;
            $orderProduct->quantity = $cartItem['qty'];

            $orderProduct->save();

        }

        return $order;

    }

    /**
     * @param Order $order
     * @return Order
     */
    public function competeOrder(Order $order): Order
    {
        $order->is_completed = 1;

        $order->save();

        return $order;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function show(int $id): Order
    {
        return Order::find($id);
    }

    public function all()
    {
        return Order::all();
    }

    /**
     * @param Order $order
     * @throws \Exception
     */
    public function destroy(Order $order): void
    {
        $order->delete();
    }
}
