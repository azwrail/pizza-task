<?php


namespace App\Http\Repositories;

use App\Models\Image;
use Illuminate\Support\Facades\Storage;

class ImageRepository
{

    private $pathToDefaultImage;

    public function __construct()
    {
        $this->pathToDefaultImage = 'https://s8.hostingkartinok.com/uploads/images/2020/02/828e6c168d4461675e57f73f5c30dc89.jpg';
    }
    /**
     * @param int $productId
     * @return Image
     */
    public function store(int $productId): Image
    {
        $image = new Image();

        if ($path = request()->input('imagePath')) {
            $image->path = $path;
        }

        else $image->path = $this->pathToDefaultImage;

        $image->product_id = $productId;

        $image->save();

        return $image;
    }
}
