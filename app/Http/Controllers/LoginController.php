<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;


class LoginController extends Controller
{

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function show()
    {
        return view('auth.auth');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        if (!auth()->attempt(request(['email', 'password']))) {
            return redirect()->back()->withErrors(['Your credentials are incorrect']);
        }

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();


        return redirect()->back();
    }
}
