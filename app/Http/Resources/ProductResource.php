<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        /**
         * @var Product $product
         */

        $product = $this;

        return [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'image' => ImageResource::make($product->image),
            'recommendedProducts' => new RecommendedProductResourceCollection($product->recommended)
        ];
    }
}
