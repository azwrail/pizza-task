<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;

class CheckPermissions
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {

        if (! auth()->user()->is_admin) {
            return redirect()->route('home')->withErrors('Sorry. Yor are not allowed there');
        }

        return $next($request);
    }
}
