<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Carbon\Carbon;
use Closure;

class CheckAuth
{
    /**
     *
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */

    public function handle($request, Closure $next)
    {
        if (! auth()->user()) {
            return redirect()->route('login')->withErrors('Sign-in and have your own profile!');
        }

        return $next($request);
    }
}
