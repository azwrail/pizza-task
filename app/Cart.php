<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart
{
    private $items = null;
    private $totalQty = 0;
    private $totalPrice = 0;

    /**
     * Cart constructor.
     * @param $oldCart
     */
    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    /**
     * @param $item
     * @param $id
     */
    public function add($item, $id)
    {
        $storedItem = ['qty' => 0, 'price' => $item->price, 'item' => $item];

        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $item->price;
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['price'] -= $this->items[$id]['item']['price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['price'];

        if ($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }

    }

    /**
     * @return array
     */
    public function getBill()
    {
        $orderCost = $this->getTotalPrice();

        $deliveryCost = $this->getDeliveryCost($orderCost);

        $totalDoll = $orderCost + $deliveryCost;

        $totalEuro = round($totalDoll*0.9);

        return ['orderCost' => $orderCost, 'deliveryCost' => $deliveryCost, 'totalDoll' => $totalDoll, 'totalEuro' => $totalEuro];
    }

    /**
     * @param int $orderCost
     * @return false|float|int
     */
    private function getDeliveryCost(int $orderCost)
    {
        if ($orderCost > 30) {
            return round($deliveryCost = $orderCost*0.1, 1);
        }

        else return $deliveryCost = 5;
    }

    /**
     * @return int
     */
    public function getTotalQty()
    {
        return $this->totalQty;
    }

    /**
     * @return int
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @return null
     */
    public function getItems()
    {
        return $this->items;
    }

}
