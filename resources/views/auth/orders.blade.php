@extends('layouts.layout')

@section('title', 'Orders')

@section('content')
    <div class="col-md-12">
        <h1>Orders</h1>
        <table class="table trans" >
            <tr>
                <th>
                    id
                </th>
                <th>
                    Phone
                </th>
                <th>
                    Order time
                </th>
                <th>
                    Status
                </th>
                <th>
                    Action
                </th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id}}</td>
                    <td>{{ $order->user_phone }}</td>
                    <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                    <td>@if ($order ? $order->is_completed == 1: '') <p class="text-success"> Completed </p> @else <p class="text-danger"> In progress </p> @endif
                        <td><div class="btn-group" role="group">
                            @if ($order ? $order->is_completed == 0: '') <a class="btn btn-success" type="button" href="{{ route('order.complete', ['order' => $order->id])}}">Complete</a> @endif
                            <a class="btn btn-warning" type="button" href="{{ route('order.show', ['order' => $order->id])}}">Open</a>

                        </div>
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
@endsection
