@extends('layouts.layout')

@section('title', 'Checkout')

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="card-header">Checkout</h1>
            <h2 class="h4"> Your order cost: {{ $bill['orderCost'] }} $ </h2>
            <h3 class="h4"> Your delivery cost: {{ $bill['deliveryCost'] }} $ </h3>
            <h4 class="h3"> Your total: {{ $bill['totalDoll'] }} $ | {{ $bill['totalEuro'] }} €</h4>
            @include('layouts.errors')
            <form method="POST" action="{{ route('order.create') }}" aria-label="Checkout">
                @csrf
                <div class="form-group">
                    <label for="phone">Your phone number</label>
                    <input id="phone" type="text" class="form-control" name="phone" @if(auth()->user() ? auth()->user()->phone: '') value="{{ auth()->user()->phone }}" @endif required autofocus>
                </div>

                    <div class="form-group">
                        <label for="address">Delivery address</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="" required autofocus >
                    </div>

                    <div class="form-group">
                        <label for="details">Contact details</label>
                        <textarea class="form-control" name="details" id="details" rows="3"></textarea>
                    </div>

                <button type="submit" onClick="ValidPhone()" class="btn btn-success">
                    Order now
                </button>
                @if(! auth()->user())
                <hr>
                <p class="text-info"> Want to have your own history of orders? <a href="{{ route('login.form') }}"> Sign in! </a> </p>
                    @endif
            </form>
        </div>
    </div>


@endsection

<script type="text/javascript">
    function ValidPhone() {
        let re = /^\d[\d\(\)\ -]{4,14}\d$/;
        let myPhone = document.getElementById('phone').value;
        let valid = re.test(myPhone);
        if (valid) output = 'Номер телефона введен правильно!';
        else output = 'Номер телефона введен неправильно!';
        document.getElementById('phone').innerHTML = document.getElementById('phone').innerHTML+'<br />'+output;
        return valid;
    }
</script>


