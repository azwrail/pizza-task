<nav class="navbar navbar-default navbar-fixed-top" style="background-color: rgba(113,202,45,0.8);">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" style="color: #d27219" href="{{ route('home') }}"> Cipolla Pizza</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('shoppingCart') }}" style="color: #d27219">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Shopping cart
                    <span class="badge"> {{ Session::has('cart') ? Session::get('cart')->gettotalQty(): '0' }} </span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #d27219">
                        <i class="fa fa-user" aria-hidden="true"></i> User Management</a>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <ul>
                            @if(!auth()->user())<li><a class="dropdown-item" href="{{ route('login.form') }}">Sign In</a></li>@endif

                            @if(!auth()->user())<li><a class="dropdown-item" href="{{ route('register.form') }}">Sign Up</a></li>@endif
                            @if(auth()->user() ? auth()->user()->is_admin: 0)<li><a class="dropdown-item" href="{{ route('product.management') }}">Products Management</a></li> @elseif(auth()->user()) <li><a class="dropdown-item" href="{{ route('profile')}}">Profile</a></li> @endif
                                @if(auth()->user() ? auth()->user()->is_admin: 0)<li><a class="dropdown-item" href="{{ route('order.index') }}">Orders Management</a></li>@endif
                                <div class="dropdown-divider"></div>
                                @if(auth()->user())<li><a class="dropdown-item" href="{{ route('logout') }}"> Logout</a></li>@endif
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
