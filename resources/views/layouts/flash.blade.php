@if ($flash = session('message'))
    <div class="col-md-4 col-md-offset-4">
    <div class="alert alert-success" role="alert">

        {{ $flash }}
    </div>
    </div>
@endif
